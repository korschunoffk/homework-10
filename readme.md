# Работа с Ansible
Для работы с ansible требуется сам ansible а так же python на сервере и хостах. (использовалась версия 2.7.5)

Структуру можно посмотреть в папке ansible



` Задача- автоматически развернуть nginx на 8080 порту . `


1. создаем inventory file 

> [web]
> nginx ansible_host=127.0.0.1 ansible_port=2222 ansible_private_key_file=.vagrant/machines/nginx/virtualbox/private_key


2. создаем ansible.cfg

```
[defaults]
roles_path = ansible/role   # в зависимости от того что будем использовать  playbook или role пишем эту строку.
inventory = staging/hosts   # ссылаемся на созданный inventory
remote_user = vagrant
host_key_checking = False
retry_files_enabled = False
```


3. создаем playbook или роль . 


> 3.1. роль  

```
---
- name: Install nginx from epel repo
  hosts: nginx
  become: true
  roles:
    - nginx

```
создаем для роли структуру папок и наполняем файлами с конф yml 



> 3.2. playbook  

```
---
- name: Install nginx from epel repo                                    # имя playbook
  hosts: nginx                                                          # хост из inventory
  become: true                                                          # стать рутом
  vars:                                                                 # блок переменных
    nginx_listen_port: 8080
  tasks:                                                                # блок задач
    - name: NGINX | Install EPEL Repo package from standart repo
      yum:
        name: epel-release
        state: present
      tags:                                                             #теги, дают возможности избирательно запускать задачи
        - epel-package
        - packages

    - name:  NGINX | Install NGINX package from EPEL Repo 
      yum:
       name: nginx
       state: present
      tags: 
       - nginx-package
       - packages
      notify:                                                           # notify - ссылки на handlers(действия), что надо сделать в конце данного task
       - restart nginx

    - name:  NGINX | Install NGINX package from EPEL Repo 
      template:
        src: templates/nginx.conf.j2
        dest: /etc/nginx/nginx.conf
      notify: 
       - reload nginx
      tags: 
       - nginx-configuration


  handlers:                                                             # описание возможных действий
    - name: restart nginx
      systemd:
        name: nginx
        state: restarted
        enabled: yes
    - name: reload nginx
      systemd:
        name: nginx
        state: reloaded
```

Запускаем ansible  playbook
```
ansible-playbook playbook.yml
```
Или роль

```
ansible-playbook role.yml
```

Проверить работоспособность gninx на нестандартном порту можно  `curl http://192.168.11.150:8080`


